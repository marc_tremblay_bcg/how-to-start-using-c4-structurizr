package io.bcgdv.howto.structurizr

import com.structurizr.Workspace
import com.structurizr.io.plantuml.PlantUMLWriter
import com.structurizr.view.View
import net.sourceforge.plantuml.FileUtils
import net.sourceforge.plantuml.SourceFileReader
import net.sourceforge.plantuml.security.SFile
import java.io.File
import java.io.FileWriter
import java.io.StringWriter
import kotlin.system.exitProcess

fun main(args: Array<String>) {
    if (args.isEmpty()) {
        println("The path to the project root must be provided.")
        exitProcess(-1)
    }

    // Good luck with that!
}

private fun filenamize(s: String, ext: String = ""): String {
    return if (s.isBlank()) {
        ""
    } else {
        val extText = if (ext.isBlank()) {
            ""
        } else {
            ".$ext"
        }
        s.replace(Regex("[-\\s:/\\\\]"), "_")
            .replace(Regex("_+"), "_") + extText
    }
}

private fun renderView(
    view: View,
    outputDirectoryPath: String
) {
    val plantUMLWriter = PlantUMLWriter()
    val viewPlantUML = StringWriter()
    plantUMLWriter.write(view, viewPlantUML)

    val plantUmlFile =
        File.createTempFile("plant-uml-renderer-", ".puml")
    val fileWriter = FileWriter(plantUmlFile)
    fileWriter.write(viewPlantUML.toString())
    fileWriter.close()

    val filename = filenamize(view.name, "png")

    val sourceFileReader = SourceFileReader(plantUmlFile)
    val image = sourceFileReader.generatedImages.first()
    val outputFile = SFile("$outputDirectoryPath/$filename")
    FileUtils.copyToFile(SFile.fromFile(image.pngFile), outputFile)
}

private fun render(workspace: Workspace, outputDirectoryPath: String) {
    val views = workspace.views

    File(outputDirectoryPath).mkdirs()

    views.views.forEach { renderView(it, outputDirectoryPath) }
}
