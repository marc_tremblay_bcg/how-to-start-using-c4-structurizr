object Dependencies {
    const val kotlinVersion = "1.4.0"
    const val ktlintVersion = "9.4.0"
    private const val plantUmlVersion = "1.2020.17"
    private const val slf4jVersion = "1.7.30"
    private const val structurizrAnnotationsVersion = "1.3.5"
    private const val structurizrPlantUmlVersion = "1.5.2"
    private const val structurizrSpringVersion = "1.3.5"
    private const val structurizrVersion = "1.6.0"

    const val plantUml = "net.sourceforge.plantuml:plantuml:$plantUmlVersion"
    const val slf4jLog4j = "org.slf4j:slf4j-log4j12:$slf4jVersion"
    const val structurizrAnnotations =
        "com.structurizr:structurizr-annotations:$structurizrAnnotationsVersion"
    const val structurizrCore =
        "com.structurizr:structurizr-core:$structurizrVersion"
    const val structurizrPlantUml =
        "com.structurizr:structurizr-plantuml:$structurizrPlantUmlVersion"
    const val structurizrSpring =
        "com.structurizr:structurizr-spring:$structurizrSpringVersion"
}