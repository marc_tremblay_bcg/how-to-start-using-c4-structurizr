# how-to-start-using-c4-structurizr

## Getting started

Execute these commands from a terminal to get started.

1. `git clone https://gitlab.com/marc_tremblay_bcg/how-to-start-using-c4-structurizr.git`
2. `brew install graphviz`
3. `./gradlew clean structurizr`

## Branches

### trunk

Minimal boiler plate to get started. Consider giving this a try if you know C4/Structurizr and want a bit more of a challenge.

### quick-start

Most people should start here as it has more boiler plate to allow one to focus on the model and views.