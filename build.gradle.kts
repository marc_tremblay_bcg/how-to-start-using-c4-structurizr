import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version Dependencies.kotlinVersion
    id("org.jlleitschuh.gradle.ktlint") version Dependencies.ktlintVersion
    id("org.jlleitschuh.gradle.ktlint-idea") version Dependencies.ktlintVersion
}

group = "io.bcgdv.howto"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    api(kotlin("stdlib-jdk8"))

    api(Dependencies.plantUml)
    api(Dependencies.slf4jLog4j)
    api(Dependencies.structurizrAnnotations)
    api(Dependencies.structurizrCore)
    api(Dependencies.structurizrPlantUml)
    api(Dependencies.structurizrSpring)
}

ktlint {
    verbose.set(true)
    outputToConsole.set(true)
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

tasks.register<JavaExec>("structurizr") {
    main = "io.bcgdv.howto.structurizr.MainKt"

    when (JavaVersion.current()) {
        JavaVersion.VERSION_1_8 -> {
            val toolsJarPath = System.getProperty("java.home").replace("/jre", "/lib/tools.jar")
            classpath(
                toolsJarPath,
                sourceSets["main"].runtimeClasspath,
                "${projectDir.absolutePath}/spring-petclinic/classes",
                fileTree(mapOf("dir" to "${projectDir.absolutePath}/spring-petclinic/lib", "include" to "*.jar"))
            )
        }

        JavaVersion.VERSION_1_9,
        JavaVersion.VERSION_1_10,
        JavaVersion.VERSION_11,
        JavaVersion.VERSION_12 -> {
            classpath(
                sourceSets["main"].runtimeClasspath,
                "${projectDir.absolutePath}/spring-petclinic/classes",
                fileTree(mapOf("dir" to "${projectDir.absolutePath}/spring-petclinic/lib", "include" to "*.jar"))
            )
            jvmArgs = listOf("--add-modules", "jdk.javadoc")
        }

        JavaVersion.VERSION_HIGHER -> throw IllegalStateException("Please use JDK >= 1.8 and <= 12")
    }
    args = listOf(projectDir.absolutePath)
}
